module.exports = {
    env: {
        es2021: true,
        node: true,
    },
    extends: [
        'airbnb-base',
    ],
    parserOptions: {
        ecmaVersion: 12,
    },
    rules: {
        indent: ['error', 4],
        'class-methods-use-this': 'off',
        'quote-props': 'off',
        'no-param-reassign': 'off',
    },
    globals: {
        use: true,
    },
};
