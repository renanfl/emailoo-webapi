/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.get('/', () => ({ greeting: 'Hello world in JSON' }));

// TODO Fazer validacoes de usuario
// TODO mostrar imagens, etc... apenas das companias que o usuario faz parte

Route.post('/user', 'UserController.create');
Route.post('/login', 'UserController.login');

Route.get('/user/', 'UserController.index').middleware('auth');
Route.post('/user/:id', 'UserController.update').middleware('auth');
Route.delete('/user/:id', 'UserController.destroy').middleware('auth');
Route.get('/user/:id', 'UserController.show').middleware('auth');

Route.resource('company', 'CompanyController').apiOnly().middleware('auth');
Route.get('/company/:id/images', 'CompanyController.images').middleware('auth');
Route.get('/company/:id/variables', 'CompanyController.variables').middleware('auth');

Route.resource('project', 'ProjectController').apiOnly().middleware('auth');

Route.resource('template', 'TemplateController').apiOnly().middleware('auth');
Route.post('/template/:id/duplicate', 'TemplateController.duplicate').middleware('auth');
Route.post('/move/template/:id', 'TemplateController.move').middleware('auth');

Route.resource('image', 'ImageController').apiOnly().middleware('auth');
Route.resource('variable', 'VariableController').apiOnly().middleware('auth');
Route.resource('item', 'ItemController').apiOnly().middleware('auth');

// Search
Route.get('/search/project', 'SearchController.project').middleware('auth');
Route.get('/search/template', 'SearchController.templates').middleware('auth');
Route.get('/search/items', 'SearchController.items').middleware('auth');

Route.post('/upload/image', 'FtpController.upload').middleware('auth');
Route.delete('/upload/image', 'FtpController.destroy').middleware('auth');
