const { responseMessage } = require('./ControllerUtils');

const dbSearch = async ({
    context,
    model,
    id,
    children,
    method,
    onSucess,
}) => {
    const { response } = context;
    let search = model.query();

    // findById
    if (id) search = search.where('id', id);

    // Se tem campos filhos
    if (children && Array.isArray(children)) {
        search = children.reduce((acm, child) => acm.with(child), search);
    }

    if (children && !Array.isArray(children)) search = search.with(children);

    // Define metodo
    if (!method) {
        if (id) {
            method = 'first';
        }
    }

    const result = await search[method]();
    if (!result) return responseMessage(response, 'notFoundError');

    return onSucess(result);
};

// ==================================================================================

module.exports = {
    dbSearch,
};
