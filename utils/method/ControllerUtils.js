const ValidateMessages = require('../data/ValidateMessages');

const { validateAll } = use('Validator');

// ----------------------------------------------------------------------------------
// Aplica regras de validacao de campos do body da request

const validateFields = async (response, requestBody, rules, onSucess) => {
    const validation = await validateAll(requestBody, rules, ValidateMessages);

    if (validation.fails()) {
        response.status(400).send({ message: validation.messages() });
        return false;
    }

    return onSucess();
};

// ----------------------------------------------------------------------------------
// Resposta defaults

const serverError = (response, error) => response
    .status(500)
    .send({ error: `Erro: ${error.message}` });

const notFoundError = (response) => response
    .status(400)
    .send({ message: 'Registro não encontrado' });

const deletedResponse = (response) => response
    .status(200)
    .send({ message: 'Registro deletado' });

const responseMessage = (response, type, data) => {
    switch (type) {
    case 'notFoundError':
        notFoundError(response);
        break;
    case 'serverError':
        serverError(response, data);
        break;
    case 'deleted':
        deletedResponse(response);
        break;

    default:
        notFoundError(response);
        break;
    }
};

// ----------------------------------------------------------------------------------
// Engloba uma request em um try/catch
// setando uma mensagem de server error padrao

const tryRequest = (response, cb) => {
    try {
        return cb();
    } catch (error) {
        return responseMessage(response, 'serverError', error);
    }
};

// ----------------------------------------------------------------------------------
// Sincroniza dinamicamente o metodo de cada Model
// EX: preciso importar o metodo .users()
// apenas passo o nome do metodo que a a funcao abaixo vincula esse metodo

const syncModelMethod = (current, childName) => current[childName]();

// ----------------------------------------------------------------------------------
// Popula uma tabela com seus filhos (relationship)

const varToString = (varObj) => Object.keys(varObj)[0];

const saveChild = async (current, item) => {
    const name = await varToString(item);
    const model = item[name];

    if (model && model.length > 0) {
        const instance = syncModelMethod(current, name);
        await instance.attach(model);
        await current.load(name);
        return current;
    }

    return false;
};

const updateChild = async (current, item) => {
    const name = await varToString(item);
    const model = item[name];

    if (model && model.length > 0) {
        const instance = syncModelMethod(current, name);
        await instance.sync(model);
        return current;
    }

    return false;
};
// ==================================================================================

module.exports = {
    responseMessage,
    validateFields,
    tryRequest,
    saveChild,
    updateChild,
};
