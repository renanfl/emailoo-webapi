module.exports = {
    'required': 'Esse campo é obrigatório',
    'username.min': 'O username deve ter mais que 5 caracteres',
    'username.unique': 'Esse usuário já existe',
    'password.min': 'A senha deve ter mais que 6 caracteres',
    'email.required': 'Esse campo é obrigatório',
    'email.email': 'O e-mail está com um formato incorreto',
    'email.unique': 'Esse e-mail já existe',
    'name.min': 'O nome deve ter mais que 4 caracteres',
};
