const {
    tryRequest,
    validateFields,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const Item = use('App/Models/Item');
const Groupitem = use('App/Models/Groupitem');
const Subtitle = use('App/Models/Subtitle');
const ImageItem = use('App/Models/Imageitem');
const MenuItem = use('App/Models/Menuitem');
const Button = use('App/Models/Button');
const Text = use('App/Models/Text');

class ItemController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Item,
                method: 'fetch',
                onSucess: (item) => item,
            }));
    }

    async storeProp(fields, itemId) {
        const {
            type,
            width,
            height,
            padding,
            bgColor,
            style,
            align,
            text,
            imageId,
            href,
            fontType,
        } = fields;

        const attrBase = {
            item_id: itemId,
            width,
            height,
            padding,
            bg_color: bgColor,
            style,
            align,
        };

        const attrSubtitle = {
            item_id: itemId,
            text,
        };

        let prop;

        if (['gallery', 'menu', 'line', 'column'].includes(type)) {
            prop = await Groupitem.create({
                ...attrBase,
                type,
            });
        } else if (type === 'subtitle') {
            prop = await Subtitle.create(attrSubtitle);
        } else if (type === 'image') {
            prop = await ImageItem.create({
                ...attrBase,
                image_id: imageId,
            });
        } else if (type === 'menu-item') {
            prop = await MenuItem.create({
                ...attrBase,
                font_type: fontType,
                text,
                href,
            });
        } else if (type === 'button') {
            prop = await Button.create({
                ...attrBase,
                font_type: fontType,
                text,
                href,
            });
        } else if (type === 'text') {
            prop = await Text.create({
                ...attrBase,
                font_type: fontType,
                text,
            });
        }

        return prop;
    }

    async store({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only([
                'name',
                'parent',
                'type',
                'template',
                'order',

                // props
                'width',
                'height',
                'padding',
                'bgColor',
                'style',
                'align',
                'text',
                'imageId',
                'href',
                'fontType',
            ]);

            const {
                name,
                parent,
                type,
                template,
                order,
            } = fields;

            // eslint-disable-next-line camelcase
            const props_id = 1;

            const rules = {
                name: 'required|min:3',
                type: 'required',
                template: 'required',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const item = await Item.create({
                        name,
                        type,
                        props_id,
                        parent_id: parent || 1,
                        template_id: template,
                        order: order || 99,
                        active: true,
                    });

                    const prop = await this.storeProp(fields, item.id);

                    if (prop.id) {
                        item.merge({ props_id: prop.id });
                        await item.save();

                        return {
                            ...item.$attributes,
                            props: prop,
                        };
                    }

                    return item;
                },
            );
        });
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Item,
                id: params.id,
                onSucess: (item) => item,
            }));
    }

    async update({ params, request, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Item,
                id: params.id,
                onSucess: async (item) => {
                    const data = request.only(['name', 'order', 'active']);

                    const { name, order, active } = data;

                    item.merge({
                        name,
                        active,
                        order,
                    });

                    await item.save();
                    return item;
                },
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Item,
                id: params.id,
                onSucess: async (item) => {
                    await item.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }
}

module.exports = ItemController;
