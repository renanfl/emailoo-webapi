const {
    tryRequest,
    validateFields,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const Image = use('App/Models/Image');

class ImageController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Image,
                method: 'fetch',
                onSucess: (image) => image,
            }));
    }

    async store({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only(['url', 'company', 'size']);

            const {
                url,
                company,
                size,
            } = fields;

            const rules = {
                url: 'required|min:20',
                company: 'required',
                size: 'required',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const image = await Image.create({
                        image_url: url,
                        company_id: company,
                        size,
                    });
                    return image;
                },
            );
        });
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Image,
                id: params.id,
                onSucess: (image) => image,
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Image,
                id: params.id,
                onSucess: async (image) => {
                    await image.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }
}

module.exports = ImageController;
