/* eslint-disable no-bitwise */
const Helpers = use('Helpers');
const EasyFtp = require('easy-ftp');
const { readdir, unlink } = require('fs/promises');

const Servers = require('../../../config/ftps-2b.json').FileZilla3.Servers.Server; // LISTA COM AS CONFIGURACOES DE FTP

class FtpController {
    uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            const r = Math.random() * 16 | 0;
            const v = c === 'x' ? r : (r & 0x3 || 0x8);
            return v.toString(16);
        });
    }

    async upload({ response, request }) {
        try {
            const profilePic = request.file('image', {
                types: ['image'],
                size: '2mb',
            });

            const { company } = request.only(['company']);

            await profilePic.move(Helpers.tmpPath('uploads'), {
                name: `${this.uuid()}.jpg`,
                overwrite: true,
            });

            if (!profilePic.moved()) {
                return profilePic.error();
            }

            const save = new Promise((resolve, reject) => {
                this.SaveToFTP(company, (err, files) => {
                    if (!err) resolve(files);
                    else reject();
                });
            });

            let files = await Promise.resolve(save);

            files = files.map((file) => `http://agencia2bdigital.com${file.remote}`);

            return response.status(200).send({ message: 'Arquivo salvo!', files });
        } catch (error) {
            return response.status(500).send({ message: 'Erro ao salvar o arquivo!' });
        }
    }

    async destroy({ request, response }) {
        try {
            const { path } = request.only(['path']);

            const save = new Promise((resolve, reject) => {
                this.RemoveToFTP(path, (err) => {
                    if (!err) resolve();
                    else reject();
                });
            });

            await Promise.resolve(save);

            return response.status(200).send({ message: 'Arquivo removido!', path });
        } catch (error) {
            return response.status(500).send({ message: 'Erro ao deletar o arquivo!' });
        }
    }

    async SaveToFTP(company, cb) {
        const images = await readdir('./tmp/uploads/');
        const remoteDir = `/emailoo/${company}/`;

        const files = images.map((file) => ({
            local: `${__dirname}\\..\\..\\..\\tmp\\uploads\\${file}`,
            remote: `${remoteDir}${file}`,
            file,
        }));

        if (files.length > 0) {
            const ag2bdigital = this.getServerByName('2bdigital');
            const ftp = new EasyFtp();

            // connect
            await new Promise((resolve, reject) => {
                ftp.connect(ag2bdigital);
                ftp.cd('/', (err) => {
                    if (!err) resolve();
                    else reject();
                });
            })
                .catch(() => cb(true));

            // Cria repositorio se ele nao existe
            await new Promise((resolve, reject) => {
                ftp.mkdir(remoteDir, (err) => {
                    if (!err) resolve();
                    else reject();
                });
            })
                .catch(() => cb(true));

            await new Promise((resolve, reject) => {
                ftp.upload(files, (err) => {
                    if (err) reject();
                    else {
                        resolve();
                    }
                    ftp.close();
                });
            })
                .then(() => {
                    files.forEach((file) => unlink(file.local));
                    cb(false, files);
                })
                .catch(() => cb(true));
        }

        return false;
    }

    async RemoveToFTP(path, cb) {
        const ag2bdigital = this.getServerByName('2bdigital');
        const ftp = new EasyFtp();

        const [http, pathRelative] = path.split('agencia2bdigital.com');

        // connect
        await new Promise((resolve, reject) => {
            ftp.connect(ag2bdigital);
            ftp.cd('/', (err) => {
                if (!err) resolve();
                else reject();
            });
        })
            .catch(() => cb(true));

        await new Promise((resolve, reject) => {
            ftp.rm(pathRelative, (err) => {
                if (!err) resolve();
                else reject();
            });
        })
            .then(() => {
                cb(false);
            })
            .catch(() => cb(true));
    }

    getServerByName(serverName) {
        let sver = Servers.filter((server) => server.Name === serverName);
        if (sver.length < 0) return false;
        [sver] = sver;

        return {
            host: sver.Host,
            port: sver.Port,
            username: sver.User,
            password: String(Buffer.from(sver.Pass.$t, sver.Pass.encoding)),
            type: 'ftp',
        };
    } // getServerByName
}

module.exports = FtpController;
