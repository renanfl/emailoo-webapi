const {
    tryRequest,
    validateFields,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const Project = use('App/Models/Project');

class ProjectController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Project,
                children: ['users', 'companies'],
                method: 'fetch',
                onSucess: (project) => project,
            }));
    }

    async store({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only(['name', 'icon', 'company', 'creator']);

            const {
                name,
                icon,
                company,
                creator,
            } = fields;

            const rules = {
                name: 'required|min:5',
                company: 'required',
                creator: 'required',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const projects = await Project.create({
                        name,
                        icon,
                        company_id: company,
                        creator,
                    });

                    await projects.loadMany(['companies', 'users']);
                    return projects;
                },
            );
        });
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Project,
                id: params.id,
                children: ['users', 'companies', 'templates'],
                onSucess: (project) => project,
            }));
    }

    async update({ params, request, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Project,
                id: params.id,
                onSucess: async (project) => {
                    const data = request.only(['name', 'icon']);
                    project.merge(data);
                    await project.save();
                    return project;
                },
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Project,
                id: params.id,
                onSucess: async (project) => {
                    await project.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }
}

module.exports = ProjectController;
