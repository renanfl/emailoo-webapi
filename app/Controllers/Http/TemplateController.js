const {
    tryRequest,
    validateFields,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const Template = use('App/Models/Template');

class TemplateController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Template,
                method: 'fetch',
                onSucess: (template) => template,
            }));
    }

    async store({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only(['name', 'project', 'subtemplate', 'creator']);

            const {
                name,
                project,
                creator,
                subtemplate,
            } = fields;

            const rules = {
                name: 'required|min:5',
                project: 'required',
                creator: 'required',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const template = await Template.create({
                        name,
                        project_id: project,
                        is_subtemplate: subtemplate,
                        creator,
                        active: true,
                    });
                    return template;
                },
            );
        });
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Template,
                id: params.id,
                onSucess: (template) => template,
            }));
    }

    async duplicate({
        request, params, response, auth,
    }) {
        return tryRequest(response,
            async () => {
                const data = request.only(['project']);
                const { project } = data;
                const currentTemplate = await Template.find(params.id);
                // eslint-disable-next-line camelcase
                const { name, is_subtemplate } = currentTemplate;

                const template = await Template.create({
                    name: `[COPIA] ${name}`,
                    is_subtemplate,
                    project_id: project,
                    creator: auth.user.id,
                    active: true,
                });
                return template;
            });
    }

    async move({
        request, params, response, auth,
    }) {
        return tryRequest(response,
            async () => {
                const data = request.only(['project']);
                const { project } = data;
                const currentTemplate = await Template.find(params.id);
                // eslint-disable-next-line camelcase
                const { name, is_subtemplate } = currentTemplate;

                const template = await Template.create({
                    name: `${name}`,
                    is_subtemplate,
                    project_id: project,
                    creator: auth.user.id,
                    active: true,
                });

                await currentTemplate.delete();

                return template;
            });
    }

    async update({ params, request, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Template,
                id: params.id,
                onSucess: async (template) => {
                    const data = request.only(['name', 'subtemplate', 'active']);

                    const { name, subtemplate, active } = data;

                    template.merge({
                        name,
                        active,
                        'is_subtemplate': subtemplate,
                    });
                    await template.save();
                    return template;
                },
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Template,
                id: params.id,
                onSucess: async (template) => {
                    await template.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }
}

module.exports = TemplateController;
