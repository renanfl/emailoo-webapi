const {
    tryRequest,
    validateFields,
    saveChild,
    updateChild,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const Company = use('App/Models/Company');

class CompanyController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Company,
                children: ['users', 'projects'],
                method: 'fetch',
                onSucess: (company) => company,
            }));
    }

    async store({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only(['name', 'users', 'projects']);
            const { users, projects, ...data } = fields;

            const rules = {
                name: 'required|min:5',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const company = await Company.create({ ...data, active: true });
                    await saveChild(company, { users });
                    await saveChild(company, { projects });
                    return company;
                },
            );
        });
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Company,
                id: params.id,
                children: ['users', 'projects'],
                onSucess: (company) => company,
            }));
    }

    async update({ params, request, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Company,
                id: params.id,
                onSucess: async (company) => {
                    const { users, ...data } = request.only(['name', 'users']);
                    company.merge(data);
                    await company.save();
                    await updateChild(company, { users });
                    return company;
                },
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Company,
                id: params.id,
                onSucess: async (company) => {
                    await company.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }

    async images({ params, response }) {
        return tryRequest(response,
            async () => {
                const company = await Company.find(params.id);
                const images = await company.images().fetch();
                return images;
            });
    }

    async variables({ params, response }) {
        return tryRequest(response,
            async () => {
                const company = await Company.find(params.id);
                const variables = await company.variables().fetch();
                return variables;
            });
    }
}

module.exports = CompanyController;
