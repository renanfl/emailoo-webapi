const {
    tryRequest,
    validateFields,
    saveChild,
    updateChild,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const User = use('App/Models/User');

class UserController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: User,
                children: ['companies'],
                method: 'fetch',
                onSucess: (user) => user,
            }));
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: User,
                id: params.id,
                children: ['companies'],
                onSucess: (user) => user,
            }));
    }

    async create({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only(['username', 'email', 'password', 'companies']);
            const { companies, ...data } = fields;

            const rules = {
                username: 'required|min:5|unique:users',
                email: 'required|email|unique:users',
                password: 'required|min:6',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const user = await User.create(data);
                    await saveChild(user, { companies });
                    return user;
                },
            );
        });
    }

    async login({ request, response, auth }) {
        return tryRequest(response, async () => {
            const { email, password } = request.only(['email', 'password']);
            const validaToken = await auth.attempt(email, password);
            return validaToken;
        });
    }

    async update({ params, request, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: User,
                id: params.id,
                onSucess: async (user) => {
                    const { companies, ...data } = request.only(['username', 'companies', 'password']);

                    // Update Children
                    user.merge(data);
                    await user.save();
                    await updateChild(user, { companies });

                    return user;
                },
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: User,
                id: params.id,
                onSucess: async (user) => {
                    await user.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }
}

module.exports = UserController;
