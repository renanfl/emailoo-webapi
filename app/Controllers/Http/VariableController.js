const {
    tryRequest,
    validateFields,
    responseMessage,
} = require('../../../utils/method/ControllerUtils');

const { dbSearch } = require('../../../utils/method/DatabaseUtil');

const Variable = use('App/Models/Variable');

class VariableController {
    async index({ response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Variable,
                method: 'fetch',
                onSucess: (variable) => variable,
            }));
    }

    async store({ request, response }) {
        return tryRequest(response, async () => {
            const fields = request.only(['key', 'value', 'type', 'company']);

            const {
                key,
                value,
                type,
                company,
            } = fields;

            const rules = {
                key: 'required|min:3',
                value: 'required',
                type: 'required',
                company: 'required',
            };

            return validateFields(
                response,
                fields,
                rules,
                async () => {
                    // Create
                    const variable = await Variable.create({
                        key,
                        value,
                        type,
                        company_id: company,
                    });
                    return variable;
                },
            );
        });
    }

    async show({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Variable,
                id: params.id,
                onSucess: (variable) => variable,
            }));
    }

    async update({ params, request, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Variable,
                id: params.id,
                onSucess: async (variable) => {
                    const data = request.only(['value']);

                    const { value } = data;

                    variable.merge({
                        value,
                    });

                    await variable.save();
                    return variable;
                },
            }));
    }

    async destroy({ params, response }) {
        return tryRequest(response,
            async () => dbSearch({
                context: { response },
                model: Variable,
                id: params.id,
                onSucess: async (variable) => {
                    await variable.delete();
                    return responseMessage(response, 'deleted');
                },
            }));
    }
}

module.exports = VariableController;
