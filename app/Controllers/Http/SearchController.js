const {
    tryRequest,
    validateFields,
} = require('../../../utils/method/ControllerUtils');

const Database = use('Database');
const Item = use('App/Models/Item');
const Image = use('App/Models/Image');

class SearchController {
    async project({ request, response, auth }) {
        return tryRequest(response,
            async () => {
                const fields = request.only(['text']);

                let companies = await auth.user.companies().fetch();
                companies = JSON.stringify(companies);
                companies = JSON.parse(companies);
                companies = companies.map((item) => item.id);

                let projects = Database
                    .from('projects');

                const { text } = fields;

                if (text && text.length > 0) {
                    projects = projects.whereRaw('LOWER(name) LIKE ?', `%${text.toLowerCase()}%`);
                }

                projects = await projects.whereIn('company_id', companies);

                return projects;
            });
    }

    async templates({ request, response }) {
        return tryRequest(response,
            async () => {
                const fields = request.only(['text', 'project', 'subtemplate']);

                const rules = {
                    project: 'required',
                };

                return validateFields(
                    response,
                    fields,
                    rules,
                    async () => {
                        let projects = Database
                            .from('templates');

                        const { text, project, subtemplate } = fields;

                        if (text && text.length > 0) {
                            projects = projects.whereRaw('LOWER(name) LIKE ?', `%${text.toLowerCase()}%`);
                        }

                        if (subtemplate) {
                            projects = projects.where('is_subtemplate', subtemplate);
                        }

                        projects = await projects.where('project_id', project);

                        return projects;
                    },
                );
            });
    }

    async items({ request, response }) {
        return tryRequest(response,
            async () => {
                const fields = request.only(['template', 'parent', 'prop']);

                const rules = {
                    template: 'required',
                };

                return validateFields(
                    response,
                    fields,
                    rules,
                    async () => {
                        const { template, parent } = fields;

                        let items = Item.query().where('template_id', template);

                        if (parent) items = items.where('parent_id', parent);

                        [
                            'groupitems',
                            'subtitles',
                            'imageitems',
                            'menuitems',
                            'buttons',
                            'texts',
                        ].forEach((propLoad) => items.with(propLoad));

                        let result = await items.fetch();

                        result = JSON.stringify(result);
                        result = JSON.parse(result);

                        result = result.map((item) => {
                            let propLoad;

                            if (['gallery', 'menu', 'line', 'column'].includes(item.type)) propLoad = 'groupitems';
                            else if (item.type === 'subtitle') propLoad = 'subtitles';
                            else if (item.type === 'image') propLoad = 'imageitems';
                            else if (item.type === 'menu-item') propLoad = 'menuitems';
                            else if (item.type === 'button') propLoad = 'buttons';
                            else if (item.type === 'text') propLoad = 'texts';
                            else propLoad = item.type;

                            let props;

                            if (item[propLoad]) {
                                props = Object
                                    .keys(item[propLoad])
                                    .filter((key) => !['item_id', 'created_at', 'updated_at', 'type'].includes(key))
                                    .reduce((acm, key) => ({
                                        ...acm,
                                        [key]: item[propLoad][key],
                                    }), {});
                            }

                            return {
                                ...item,
                                props,
                                groupitems: undefined,
                                subtitles: undefined,
                                imageitems: undefined,
                                menuitems: undefined,
                                buttons: undefined,
                                texts: undefined,
                            };
                        });

                        result = await this.getImage(result);

                        result = await Promise.all([...result]);

                        return result;
                    },
                );
            });
    }

    async getImage(result) {
        const response = await result.map(async (itemResult) => {
            if (itemResult.type === 'image' && itemResult.props) {
                const images = await Image.find(itemResult.props.image_id);
                itemResult.props.href = images.$attributes.image_url;
                return itemResult;
            }

            return itemResult;
        });

        return response;
    }
}

module.exports = SearchController;
