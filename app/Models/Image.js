/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Image extends Model {
    companies() {
        return this.hasOne('App/Models/Company', 'company_id', 'id');
    }

    items() {
        return this.belongsToMany('App/Models/Imageitem', 'image_id', 'id');
    }
}

module.exports = Image;
