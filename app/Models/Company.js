/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Company extends Model {
    users() {
        return this.belongsToMany('App/Models/User');
    }

    projects() {
        return this.hasMany('App/Models/Project', 'id', 'company_id');
    }

    images() {
        return this.hasMany('App/Models/Image', 'id', 'company_id');
    }

    variables() {
        return this.hasMany('App/Models/Variable', 'id', 'company_id');
    }
}

module.exports = Company;
