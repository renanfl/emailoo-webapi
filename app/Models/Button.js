/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Button extends Model {
    items() {
        return this.belongsTo('App/Models/Item', 'props_id', 'id');
    }
}

module.exports = Button;
