/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Item extends Model {
    templates() {
        return this.hasOne('App/Models/Template', 'template_id', 'id');
    }

    groupitems() {
        return this.hasOne('App/Models/Groupitem', 'props_id', 'id');
    }

    subtitles() {
        return this.hasOne('App/Models/Subtitle', 'props_id', 'id');
    }

    imageitems() {
        return this.hasOne('App/Models/Imageitem', 'props_id', 'id');
    }

    menuitems() {
        return this.hasOne('App/Models/Menuitem', 'props_id', 'id');
    }

    buttons() {
        return this.hasOne('App/Models/Button', 'props_id', 'id');
    }

    texts() {
        return this.hasOne('App/Models/Text', 'props_id', 'id');
    }
}

module.exports = Item;
