/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Groupitem extends Model {
    items() {
        return this.belongsTo('App/Models/Item', 'props_id', 'id');
    }
}

module.exports = Groupitem;
