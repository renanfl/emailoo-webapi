/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Template extends Model {
    projects() {
        return this.hasOne('App/Models/Project', 'project_id', 'id');
    }

    users() {
        return this.hasOne('App/Models/User', 'creator', 'id');
    }

    items() {
        return this.hasMany('App/Models/Item', 'id', 'template_id');
    }
}

module.exports = Template;
