/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Imageitem extends Model {
    items() {
        return this.belongsTo('App/Models/Item', 'props_id', 'id');
    }

    images() {
        return this.hasOne('App/Models/Image', 'image_id', 'id');
    }
}

module.exports = Imageitem;
