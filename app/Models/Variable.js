/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Variable extends Model {
    companies() {
        return this.hasOne('App/Models/Company', 'company_id', 'id');
    }
}

module.exports = Variable;
