/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Project extends Model {
    companies() {
        return this.hasOne('App/Models/Company', 'company_id', 'id');
    }

    users() {
        return this.hasOne('App/Models/User', 'creator', 'id');
    }

    templates() {
        return this.hasMany('App/Models/Template', 'id', 'project_id');
    }
}

module.exports = Project;
