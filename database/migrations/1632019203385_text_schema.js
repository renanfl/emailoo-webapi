/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class TextSchema extends Schema {
    up() {
        this.create('texts', (table) => {
            table.increments();

            table
                .integer('item_id')
                .unsigned()
                .references('id')
                .inTable('items')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table
                .integer('width')
                .notNullable();

            table
                .integer('height')
                .notNullable();

            table
                .string('padding')
                .notNullable();

            table
                .string('align')
                .notNullable();

            table
                .string('bg_color')
                .nullable();

            table
                .string('style')
                .nullable();

            table
                .string('text')
                .notNullable();

            table
                .string('font_type')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('texts');
    }
}

module.exports = TextSchema;
