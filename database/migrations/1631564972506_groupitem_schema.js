/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class GroupitemSchema extends Schema {
    up() {
        this.create('groupitems', (table) => {
            table.increments();

            table
                .string('type')
                .notNullable();

            table
                .integer('item_id')
                .unsigned()
                .notNullable();

            table
                .integer('width')
                .notNullable();

            table
                .integer('height')
                .nullable();

            table
                .string('padding')
                .notNullable();

            table
                .string('bg_color')
                .notNullable();

            table
                .string('style')
                .notNullable();

            table
                .string('align')
                .defaultTo('center')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('groupitems');
    }
}

module.exports = GroupitemSchema;
