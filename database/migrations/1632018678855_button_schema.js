/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ButtonSchema extends Schema {
    up() {
        this.create('buttons', (table) => {
            table.increments();

            table
                .integer('item_id')
                .unsigned()
                .references('id')
                .inTable('items')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table
                .integer('width')
                .notNullable();

            table
                .integer('height')
                .notNullable();

            table
                .string('padding')
                .notNullable();

            table
                .string('align')
                .notNullable();

            table
                .string('bg_color')
                .nullable();

            table
                .string('style')
                .nullable();

            table
                .string('href')
                .notNullable();

            table
                .string('text')
                .notNullable();

            table
                .string('font_type')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('buttons');
    }
}

module.exports = ButtonSchema;
