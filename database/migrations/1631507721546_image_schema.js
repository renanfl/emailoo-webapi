const Schema = use('Schema');

class ImageSchema extends Schema {
    up() {
        this.create('images', (table) => {
            table.increments();

            table
                .string('image_url')
                .notNullable();

            table
                .integer('company_id')
                .unsigned()
                .references('id')
                .inTable('companies')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .notNullable();

            table
                .float('size')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('images');
    }
}

module.exports = ImageSchema;
