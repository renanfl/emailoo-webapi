/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ProjectSchema extends Schema {
    up() {
        this.create('projects', (table) => {
            table.increments();

            table
                .string('icon')
                .nullable();

            table
                .string('name')
                .notNullable();

            table
                .integer('company_id')
                .unsigned()
                .references('id')
                .inTable('companies')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .notNullable();

            table
                .integer('creator')
                .unsigned()
                .references('id')
                .inTable('users')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('projects');
    }
}

module.exports = ProjectSchema;
