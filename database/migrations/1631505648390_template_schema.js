/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class TemplateSchema extends Schema {
    up() {
        this.create('templates', (table) => {
            table.increments();

            table
                .string('name')
                .notNullable();

            table
                .integer('project_id')
                .unsigned()
                .references('id')
                .inTable('projects')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .notNullable();

            table
                .boolean('is_subtemplate')
                .defaultTo(false)
                .notNullable();

            table
                .integer('creator')
                .unsigned()
                .references('id')
                .inTable('users')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .notNullable();

            table
                .boolean('active')
                .defaultTo(true)
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('templates');
    }
}

module.exports = TemplateSchema;
