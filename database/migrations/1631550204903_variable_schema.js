/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class VariableSchema extends Schema {
    up() {
        this.create('variables', (table) => {
            table.increments();

            table
                .string('key')
                .notNullable();

            table
                .string('value')
                .notNullable();

            table
                .integer('company_id')
                .unsigned()
                .references('id')
                .inTable('companies')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .notNullable();

            table
                .string('type')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('variables');
    }
}

module.exports = VariableSchema;
