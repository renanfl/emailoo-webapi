/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class CompanyUserSchema extends Schema {
    up() {
        this.create('company_user', (table) => {
            table.increments();

            table
                .integer('user_id')
                .unsigned()
                .references('id')
                .inTable('users')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table
                .integer('company_id')
                .unsigned()
                .references('id')
                .inTable('companies')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table.unique(['user_id', 'company_id']);

            table.timestamps();
        });
    }

    down() {
        this.drop('company_user');
    }
}

module.exports = CompanyUserSchema;
