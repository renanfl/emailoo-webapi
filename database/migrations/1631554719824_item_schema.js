/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ItemSchema extends Schema {
    up() {
        this.create('items', (table) => {
            table.increments();

            table
                .string('name')
                .notNullable();

            table
                .string('type')
                .notNullable();

            table
                .integer('props_id')
                .notNullable();

            table
                .integer('parent_id')
                .unsigned()
                .references('id')
                .onUpdate('CASCADE')
                .onDelete('CASCADE')
                .nullable();

            table
                .integer('template_id')
                .unsigned()
                .references('id')
                .inTable('templates')
                .onDelete('CASCADE')
                .notNullable();

            table
                .integer('order')
                .notNullable();

            table
                .boolean('active')
                .defaultTo(true)
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('items');
    }
}

module.exports = ItemSchema;
