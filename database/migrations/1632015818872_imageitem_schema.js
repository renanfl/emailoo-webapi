/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ImageitemSchema extends Schema {
    up() {
        this.create('imageitems', (table) => {
            table.increments();

            table
                .integer('item_id')
                .unsigned()
                .references('id')
                .inTable('items')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table
                .integer('image_id')
                .unsigned()
                .references('id')
                .inTable('images')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table
                .integer('width')
                .notNullable();

            table
                .integer('height')
                .notNullable();

            table
                .string('padding')
                .notNullable();

            table
                .string('align')
                .notNullable();

            table
                .string('bg_color')
                .nullable();

            table
                .string('style')
                .nullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('imageitems');
    }
}

module.exports = ImageitemSchema;
