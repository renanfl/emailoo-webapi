const Schema = use('Schema');

class SubtitleSchema extends Schema {
    up() {
        this.create('subtitles', (table) => {
            table.increments();

            table
                .integer('item_id')
                .unsigned()
                .references('id')
                .inTable('items')
                .onUpdate('CASCADE')
                .onDelete('CASCADE');

            table
                .string('text')
                .notNullable();

            table.timestamps();
        });
    }

    down() {
        this.drop('subtitles');
    }
}

module.exports = SubtitleSchema;
